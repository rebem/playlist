const playLists = [];
function addPlaylist(playList) {
  playLists.push(playList);
}
addPlaylist({
  id: 0,
  name: "Exclusive",
  songs: [{ name: "Big fish", artist: "Chance" }],
});
console.log(playLists);

function createPlaylist(name) {
  return {
    id: 0,
    name: name,
    songs: [{ name: "Title", artist: "" }],
  };
}

function getPlaylist(name) {
  for (let i = 0; i < playLists.length; i++) {
    if (playLists[i].name === name) {
      return playLists[i];
    }
    i++;
  }
  return "Playlist not found";
}

const playListName1 = prompt("What is the name of your playlist?");

const playListName2 = prompt("What is the name of your playlist?");

// const playList = {
//   id: 0,
//   name: "Exclusive",
//   songs: [{ name: "Big Fish", artist: "Chance" }],
// };

const playList1 = createPlaylist(playListName1);
const playList2 = createPlaylist(playListName2);

addPlaylist(playList1);
addPlaylist(playList2);

console.log(playLists);

getPlaylist("Exclusive");
